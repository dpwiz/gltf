module Codec.GlTF.Texture
  ( TextureIx(..)
  , Texture(..)
  ) where

import Codec.GlTF.Prelude

import Codec.GlTF.Image (ImageIx)
import Codec.GlTF.Sampler (SamplerIx)

newtype TextureIx = TextureIx { unTextureIx :: Int }
  deriving (Eq, Ord, Show, FromJSON, ToJSON, Generic)

data Texture = Texture
  { sampler    :: Maybe SamplerIx
  , source     :: Maybe ImageIx
  , name       :: Maybe Text
  , extensions :: Maybe Object
  , extras     :: Maybe Value
  } deriving (Eq, Show, Generic)

instance FromJSON Texture
instance ToJSON Texture
