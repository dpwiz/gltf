module Codec.GlTF.Image
  ( ImageIx(..)
  , Image(..)
  ) where

import Codec.GlTF.Prelude

import Codec.GlTF.BufferView (BufferViewIx)
import Codec.GlTF.URI (URI)

newtype ImageIx = ImageIx { unImageIx :: Int }
  deriving (Eq, Ord, Show, FromJSON, ToJSON, Generic)

-- | Image data used to create a texture.
--
-- Image can be referenced by URI or bufferView index.
-- @mimeType@ is required in the latter case.
data Image = Image
  { uri        :: Maybe URI
  , mimeType   :: Maybe Text
  , bufferView :: Maybe BufferViewIx
  , name       :: Maybe Text
  , extensions :: Maybe Object
  , extras     :: Maybe Value
  } deriving (Eq, Show, Generic)

instance FromJSON Image
instance ToJSON Image
