module Codec.GlTF
  ( GlTF(..)
  , fromByteString
  , fromFile
  , fromChunk
  ) where

import Data.ByteString (ByteString)

import qualified Data.Aeson as JSON

import Codec.GlTF.Root (GlTF(..))

import qualified Codec.GLB  as GLB

fromByteString :: ByteString -> Either String GlTF
fromByteString = JSON.eitherDecodeStrict'

fromFile :: FilePath -> IO (Either String GlTF)
fromFile = JSON.eitherDecodeFileStrict'

fromChunk :: GLB.Chunk -> Either String GlTF
fromChunk = JSON.eitherDecodeStrict' . GLB.chunkData
